import React, { useState, useEffect } from "react";
import { get, post } from "./Requests";

export const Form = ({ form }) => {
  const [typesFieldsMapper, setTypesFieldsMapper] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [formDetails, setFormDetails] = useState({'Pickup Location': 'gampaha'});
  useEffect(() => onLoad(), []);
  useEffect(() => setFormDetails({}), [form]);

  const title = form.name;

  console.log(form);

  const onLoad = () => {
    get(
      "formTypes",
      (props) => {
        console.log(props);
        setTypesFieldsMapper(props.data);
        setIsLoading(false);
      },
      (err) => console.log(err)
    );
  };

  const onSubmit = () => {
    const data = { formType: title, formDetails: formDetails };
    console.log(data);
    post("formSubmissions", data).catch((e) => console.log(e));
  };

  function onChange(field, e) {
    return setFormDetails((formDetails) => {
      const oldDetails = formDetails;
      oldDetails[field] = e.target.value;
      return oldDetails;
    });
  }

  if (isLoading) {
    <>Loading...</>;
  }

  return (
    <div style={{ backgroundColor: "lightgrey", width: '50%', margin: 20, padding: 10, zIndex: 100  }}>
      {/* {title} */}

      <div style={{ padding: 5 }}>
        {typesFieldsMapper
          .filter((type) => type.name === form.name)[0]
          ?.fields.map((row) => (
            <div style={{ display: "flex", padding: 5 }}>
              {row.map((f) => {
                const field = f.name;

                return (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      padding: 10,
                      backgroundColor: 'transparent'
                    }}
                  >
                    <label style={{ padding: 5, textAlign: 'left', fontWeight: 'bold', fontSize: 15}}>{`${field} : `}</label>
                    <div style={{ display: "flex", flexDirection: "row" }}>
                      {f.type === "select" ? (
                        <select
                          key={field}
                          value={formDetails[field]}
                          style={{width: 400}}
                          onChange={(e) => onChange(field, e)}
                        >
                          <option disabled selected value=''>
                            -- select an option --
                          </option>
                          {f.options.map((option) => (
                            <option key={option} value={option}>
                              {option}
                            </option>
                          ))}
                        </select>
                      ) : (
                        <input
                          key={field}
                          type={f.type}
                          style={{width: 400}}
                          defaultValue={formDetails[field]}
                          onChange={(e) =>
                            setFormDetails((formDetails) => {
                              const oldDetails = formDetails;
                              oldDetails[field] = e.target.value;
                              return oldDetails;
                            })
                          }
                        />
                      )}
                      
                    </div>
                  </div>
                );
              })}
            </div>
          ))}
      </div>
      <button onClick={onSubmit}>submit</button>
    </div>
  );
};
