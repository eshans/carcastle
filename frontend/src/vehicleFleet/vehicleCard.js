import React from "react";

export const VehicleCard = ({ vehicleDetails }) => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        padding: 10
      }}
    >
      <img src={require(`${vehicleDetails.image}`)} alt='vehicleimg' style={{width: 200}}></img>
      <div style={{ textAlign: "left", padding: 10 }}>
        <div style={{ fontWeight: "bolder" }}>{vehicleDetails.name}</div>
        <div>{vehicleDetails.method}</div>
        <div>{vehicleDetails.Transmission}</div>
        <div>{`LKR ${vehicleDetails.Rate} per extra km`}</div>
        <div>{`${vehicleDetails.Passengers} Passengers`}</div>
        {vehicleDetails.Luggage && (
          <div>{`${vehicleDetails.Luggage} Luggage`}</div>
        )}

        {vehicleDetails.AirConditioning ? <div>Air Conditioning</div> : null}
      </div>
      <div
        style={{
          height: "100%",
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-end",
        }}
      >
        <div style={{ fontWeight: "bold", margin: 5 }}>
          {`LKR ${vehicleDetails.Price}`}
        </div>
        <button style={{ margin: 5, padding: 5 }}>Book Now</button>
      </div>
    </div>
  );
};
