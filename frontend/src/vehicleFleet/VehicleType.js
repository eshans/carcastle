import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { TwoColumnGrid } from "../Components";
// import vehiclesList from "./initialVehicles.json";
import { VehicleCard } from "./vehicleCard";

export const VehicleType = () => {
  const { vehicleType } = useParams();
  const [vehiclesList, setVehiclesList] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => onLoad(), []);

  const onLoad = () => {
    const options = {
      headers: { "Content-Type": "application/json" },
      method: "GET",
    };
    fetch(`http://127.0.0.1:8000/vehicles`, options)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setVehiclesList(data.data);
        setLoading(false);
      })
      .catch((err) => console.log(err));
  };

  const filteredVehicles = vehiclesList.filter(
    (vehicle) => vehicle.type === vehicleType
  );

  if (loading) {
    return <>Loading...</>;
  }

  return (
    <div style={{backgroundColor:'rgb(204,204,0)'}}>
      <div
        style={{
          backgroundColor: "grey",
          width: "100%",
          padding: 10,
          paddingLeft: 200,
          textAlign: "left",
          fontWeight: "bolder",
        }}
      >{`${vehicleType}`}</div>
      <div></div>
      <div>
        {filteredVehicles.length === 0 ? (
          <div style={{padding: 50}}>No vehicles in this type</div>
        ) : (
          filteredVehicles.map((vehicle) => {
            return <VehicleCard vehicleDetails={vehicle} />;
          })
        )}
      </div>
    </div>
  );
};
